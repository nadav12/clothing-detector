# Clothing-Detector

An image classifier capable of receiving a poor quality 28 by 28 grayscale image of some clothing and guess what clothing item is in the image. A CNN model trained on the Fashion MNIST dataset.
